import { Link } from "react-router-dom";
import { Outlet } from "react-router-dom";

function MainLayout() {
  return (
    <>
      <header>
        <nav>
          <ul>
            <li>
              <Link to='/'>Liste des films</Link>
            </li>
            <li>
              <Link to='/create'>Créer une fiche de film</Link>
            </li>
            <li>
              <Link to='/login'>Se connecter</Link>
            </li>
          </ul>
        </nav>
      </header>
      <main>
        <Outlet />
      </main>
      <footer>
        © Simplon
      </footer>
    </>
  );
}

export default MainLayout;
