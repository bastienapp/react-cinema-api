import { useState } from "react";
import axios from "axios";

function Login() {
  const [emailValue, setEmail] = useState("");
  const [passwordValue, setPassword] = useState("");

  function handleSubmit(event) {
    event.preventDefault();
    axios
      .post("http://localhost:5000/auth/login", {
        email: emailValue,
        password: passwordValue
      })
      .then((response) => {
        const data = response.data;
        // enregistrer la session de l'utilisateur
        const sessionId = data.sessionId;
        localStorage.setItem("session_id", sessionId);
        // rediger vers l'accueil
      })
      .catch((err) => {
        if (err.response.status === 400) {
          alert('Merci de renseigner l\'email et le mot de passe');
        } else if (err.response.status === 401) {
          const message = err.response.data.error;
          if (message === 'Invalid email') {
            alert('Email incorrect')
          } else {
            alert('Mot de passe incorrect')
          }
        }
      });
  }

  return (
    <form onSubmit={handleSubmit}>
      <label>
        Email :{" "}
        <input
          type='email'
          value={emailValue}
          onChange={(event) => setEmail(event.target.value)}
        />
      </label>
      <br />
      <label>
        Mot de passe :{" "}
        <input
          type='password'
          value={passwordValue}
          onChange={(event) => setPassword(event.target.value)}
        />
      </label>
      <br />
      <button type='submit'>Se connecter</button>
    </form>
  );
}

export default Login;
