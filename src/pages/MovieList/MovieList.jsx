import { useEffect, useState } from "react";
import axios from "axios";

function MovieList() {
  const [movieList, setMovieList] = useState([]);

  useEffect(() => {
    axios.get("http://localhost:5000/movies").then((response) => {
      setMovieList(response.data);
    });
  }, []);

  return (
    <>
      <h1>Liste des films</h1>
      <ul>
        {movieList.map((movie) => (
          <li key={movie.movie_id}>{movie.title}</li>
        ))}
      </ul>
    </>
  );
}

export default MovieList;
