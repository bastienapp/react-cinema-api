import { useEffect, useState } from "react";
import "./MovieCreate.css";
import axios from "axios";

function MovieCreate() {
  const [directorList, setDirectorList] = useState([]);
  const [title, setTitle] = useState("");
  const [releaseYear, setReleaseYear] = useState("");
  const [duration, setDuration] = useState("");
  const [rate, setRate] = useState("");

  useEffect(() => {
    axios
      .get("http://localhost:5000/directors")
      .then((response) => {
        setDirectorList(response.data);
      })
      .catch((error) => console.error(error));
  }, []);

  function handleSubmit(event) {
    event.preventDefault();

    axios
      .post("http://localhost:5000/movies", {
        title: title,
        release_year: releaseYear,
        duration: duration,
        rate: rate,
      })
      .then((response) => console.log(response))
      .catch((error) => console.error(error.message));
  }

  return (
    <>
      <h1>Créer un film:</h1>
      <form onSubmit={handleSubmit}>
        <label>
          Title:{" "}
          <input
            type='text'
            value={title}
            onChange={(event) => setTitle(event.target.value)}
          />
        </label>
        <label>
          Release year:{" "}
          <input
            type='number'
            value={releaseYear}
            onChange={(event) => setReleaseYear(event.target.value)}
          />
        </label>
        <label>
          Duration:{" "}
          <input
            type='number'
            value={duration}
            onChange={(event) => setDuration(event.target.value)}
          />
        </label>
        <label>
          Rate:{" "}
          <input
            type='number'
            value={rate}
            onChange={(event) => setRate(event.target.value)}
          />
        </label>
        <label>
          Réalisateur:
          <br />
          <select name='director_id'>
            {directorList.map((director) => (
              <option key={director.director_id} value={director.director_id}>
                {director.director_fullname}
              </option>
            ))}
          </select>
        </label>
        <button type='submit'>Envoyer</button>
      </form>
    </>
  );
}

export default MovieCreate;
